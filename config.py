# ## Build configuration, tied to config.json in yocto-autobuilder-helpers
# Repositories used by each builder
buildertorepos = {
    "a-quick": ["meta-ds", "poky", "bitbake", "meta-openembedded", "meta-raspberrypi", "meta-erlang", "meta-iot-cloud", "meta-mender", "meta-swupdate"],
    "a-full": ["meta-ds", "poky", "bitbake", "meta-openembedded", "meta-raspberrypi", "meta-erlang", "meta-iot-cloud", "meta-mender", "meta-swupdate"],
    "checkuri": ["meta-ds", "poky", "bitbake", "meta-openembedded", "meta-raspberrypi", "meta-erlang", "meta-iot-cloud", "meta-mender", "meta-swupdate"],
    "check-layer": ["meta-ds", "poky", "meta-openembedded", "meta-raspberrypi", "meta-erlang", "meta-iot-cloud", "meta-mender", "meta-swupdate"],
    "default": ["meta-ds", "poky", "bitbake", "meta-openembedded", "meta-raspberrypi", "meta-erlang", "meta-iot-cloud", "meta-mender", "meta-swupdate"]
}

# Repositories used that the scripts need to know about and should be buildbot
# user customisable
repos = {
    "yocto-autobuilder-helper":
        ["ssh://git@bitbucket.org/deepseaiot/yocto-autobuilder-helper",
         "warrior", "HEAD"],
    "poky": ["git://git.yoctoproject.org/poky", "warrior", "f475afc5df0837532dcd0f3a831ddc3aec8941f1"],
    "yocto-docs": ["git://git.yoctoproject.org/yocto-docs", "warrior", "HEAD"],
    "bitbake": ["git://git.openembedded.org/bitbake", "1.42", "HEAD"],
    "meta-openembedded": ["git://git.openembedded.org/meta-openembedded", "warrior", "a24acf94d48d635eca668ea34598c6e5c857e3f8"],
    "meta-iot-cloud": ["https://github.com/intel-iot-devkit/meta-iot-cloud", "warrior", "8c6372fdfa653c7b8fac99b734b1632de15ad38c"],
    "meta-erlang": ["https://github.com/joaohf/meta-erlang", "master", "5ebb4df3963e6c42406a972afbd92765d1855b09"],
    "meta-mender": ["https://github.com/mendersoftware/meta-mender", "warrior", "ffbdad95de07ea746d8075980866edac673f58e9"],
    "meta-raspberrypi": ["https://github.com/agherzan/meta-raspberrypi", "warrior", "615a1a83399623bcb7afb500bd9d3fc633e32004"],
    "meta-ds": ["ssh://git@bitbucket.org/deepseaiot/meta-ds", "warrior-dev", "HEAD"],
    "meta-swupdate": ["https://github.com/sbabic/meta-swupdate", "warrior", "7ebe40f1a9b312476580fc74541231ac202feaa4"]
}

trigger_builders_wait_shared = [
    "ds-cm3", "ds-stratocan",
    "qemux86-64", 
    "check-layer"
]

trigger_builders_wait_quick = trigger_builders_wait_shared + [
]

trigger_builders_wait_full = trigger_builders_wait_shared + [
    "ds-os", "ds-sdk",
    "buildtools"
]

trigger_builders_wait_releases = {
    "warrior" : trigger_builders_wait_shared + []
}

# Builders which are individually triggered
builders_others = [
    "bringup",
    "wic", 
    "qemux86-64-poky"
]

subbuilders = list(set(trigger_builders_wait_quick + trigger_builders_wait_full + builders_others))
builders = ["a-quick", "a-full", "docs"] + subbuilders

# ## Cluster configuration
# Publishing settings
#sharedrepodir = "/srv/autobuilder/repos"
#publish_dest = "/srv/autobuilder/autobuilder.yoctoproject.org/pub"
sharedrepodir = "/srv/autobuilder/dsautobuilder-share/repos"
publish_dest = "/srv/autobuilder/dsautobuilder-pub"

# Web UI settings
web_port = 8010

# List of workers in the cluster
workers_ubuntu = ["ubuntu1804-ty-1","ubuntu1804-ty-2"]
workers_centos = []
workers_fedora = []
workers_debian = []
workers_opensuse = []

workers = workers_ubuntu + workers_centos + workers_fedora + workers_debian + workers_opensuse 

workers_bringup = []
workers_buildperf = []
workers_wine = ["ubuntu1804-ty-1", "ubuntu1804-ty-2", "ubuntu1804-ty-3"]
workers_constrained = ["ubuntu1804-ty-2"]
workers_buildperf = []
workers_arm = []
# workers which don't need buildtools for AUH
workers_auh = ["ubuntu1904-ty-1", "ubuntu1804-ty-1", "ubuntu1804-ty-2", "ubuntu1804-ty-3", "centos8-ty-1", "centos8-ty-2", "debian10-ty-1", "debian10-ty-2", "debian10-ty-3"]

all_workers = workers + workers_bringup + workers_buildperf + workers_arm

# Worker filtering for older releases
workers_prev_releases = {
    "dunfell" : ("ubuntu1804", "ubuntu1904", "ubuntu2004", "perf-"),
    "zeus" : ("ubuntu1804", "ubuntu1904", "perf-"),
    "warrior" : ("ubuntu1804", "ubuntu1904", "perf-"),
    "thud" : ("ubuntu1804", "ubuntu1904", "perf-"),
    "sumo" : ("ubuntu1804", "perf-")
}

# Worker configuration, all workers configured the same...
# TODO: support per-worker config
worker_password = "pass"
worker_max_builds = 3
notify_on_missing = None

# Some builders should only run on specific workers (host OS dependent)
builder_to_workers = {
    "bringup": workers,
    "a-quick": workers_constrained,
    "a-full": workers_constrained,
    "check-layer": workers_constrained,
    "default": workers
}
